// pages/linkMan/linkMan.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    linkMan:[],

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '联系人信息加载中',
    });
    let that=this;
    wx.request({
      url: 'http://localhost:8080/tbl-linkman/pageLinkman/1/-1',
      method:'GET',
      complete:function(){
        wx.hideLoading();
      },
      success:function(res){
        if(res.statusCode==200){
          console.log("成功");
          that.setData({
            linkMan:res.data.data.records,
          });
          console.log("linkMan",that.data.linkMan);
        }
        
      }
    })
   

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
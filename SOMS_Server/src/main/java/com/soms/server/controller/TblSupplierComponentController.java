package com.soms.server.controller;


import com.alibaba.druid.sql.visitor.functions.If;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.soms.server.common.JsonResult;
import com.soms.server.entity.vo.SupplierComponentVo;
import com.soms.server.service.TblSupplierComponentService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 供应商零件关系表 前端控制器
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-11
 */
@RestController
@RequestMapping("/tbl-supplier-component")
@CrossOrigin
public class TblSupplierComponentController {
    @Autowired
    TblSupplierComponentService supplierComponentService;

    @ApiOperation("分页查询供应商零件列表列表")
    @GetMapping("pageSupplierComponent/{current}/{size}")
    public JsonResult queryAllSupplierCompent(@ApiParam(name = "current", value = "当前页", required = true)
                                              @PathVariable Integer current,
                                              @ApiParam(name = "size", value = "每页结果数", required = true)
                                              @PathVariable Integer size){
        Page page = new Page(current,size);
        Page<SupplierComponentVo> page1 = supplierComponentService.pageSupplierComponent(page);
        return new JsonResult(true,page1);
    }
    @ApiOperation("根据id查询供应商零件信息")
    @GetMapping("querySupplierComponentById/{id}")
    public JsonResult querySupplierCompentById( @ApiParam(name = "id", value = "供应商零件列表id", required = true)
                                              @PathVariable Integer id){

        SupplierComponentVo supplierComponentVo = supplierComponentService.querySupplierCompentById(id);
        return new JsonResult(true,supplierComponentVo);
    }
    @ApiOperation("根据零件id查询供应商零件列表")
    @GetMapping("getOptions/{id}")
    public JsonResult getOptions(@ApiParam(name = "id", value = "零件id", required = true)
                                              @PathVariable Integer id
                                              ){
        List<SupplierComponentVo> list = supplierComponentService.querySupplierComponentByComponentId(id);
        return new JsonResult(true,list);
    }
    @ApiOperation("根据id删除供应商供应零件信息")
    @DeleteMapping("{id}")
    public JsonResult deletesupplierComponentById(@ApiParam(name = "id", value = "供应商零件表id", required = true)
                                              @PathVariable Integer id){
        // count  sql影响行数
        int count = supplierComponentService.deletesupplierComponentById(id);
        if (count>0) {
            return new JsonResult(true,count);
        }else {
            return new JsonResult(false);
        }
    }
    @ApiOperation("根据id修改供应商供应零件信息")
    @PostMapping("updateSupplierComponent")
    public JsonResult updateSupplierComponentById(@ApiParam(name = "supplierComponentVo", value = "供应商供应零件信息", required = true)
                                                      @RequestBody SupplierComponentVo supplierComponentVo){
        // count  sql影响行数
        int count = supplierComponentService.updateSupplierComponentById(supplierComponentVo);
        if (count>0) {
            return new JsonResult(true,count);
        }else {
            return new JsonResult(false);
        }
    }

}


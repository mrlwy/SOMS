package com.soms.server.service.impl;

import com.soms.server.entity.TblSupplier;
import com.soms.server.mapper.TblSupplierMapper;
import com.soms.server.service.TblSupplierService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 供应商表 服务实现类
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-11
 */
@Service
public class TblSupplierServiceImpl extends ServiceImpl<TblSupplierMapper, TblSupplier> implements TblSupplierService {

    @Autowired
    private TblSupplierMapper tblSupplierMapper;
    @Override
    public int addSupplierInfo(TblSupplier tblSupplier) {
        tblSupplierMapper.insert(tblSupplier);
        return 0;
    }

    @Override
    public int updateSupplierInfo(TblSupplier tblSupplier) {
        tblSupplierMapper.updateById(tblSupplier);
        return 0;
    }
}

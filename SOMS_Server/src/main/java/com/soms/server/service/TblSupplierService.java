package com.soms.server.service;

import com.soms.server.entity.TblSupplier;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 供应商表 服务类
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-11
 */

public interface TblSupplierService extends IService<TblSupplier> {
    /**
     * 添加供应商的信息
     * @param tblSupplier 供应商
     * @return SQL执行行数
     */
    public int addSupplierInfo(TblSupplier tblSupplier);

    /**
     * 修改供应商信息
     * @param tblSupplier 供应商信息 ，必须包含供应商id ，根据id进行修改
     * @return 执行结果
     */
    public int updateSupplierInfo(TblSupplier tblSupplier);
}

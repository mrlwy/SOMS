package com.soms.server.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.soms.server.entity.TblLinkman;
import com.baomidou.mybatisplus.extension.service.IService;
import com.soms.server.entity.vo.LinkmanVo;

/**
 * <p>
 * 供应商联系人表 服务类
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-23
 */
public interface TblLinkmanService extends IService<TblLinkman> {

    /**
     *
     * @param page 分页构造器对象
     * @return 供应商联系人分页结果
     */
    Page<LinkmanVo> pageLinkman(Page page);

    /**
     * 根据id查询联系人信息
     * @param id 联系人id
     * @return 联系人信息
     */
    LinkmanVo queryLinkmanById(Integer id);
}

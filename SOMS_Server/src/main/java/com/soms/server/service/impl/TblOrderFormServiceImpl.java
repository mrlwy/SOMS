package com.soms.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.soms.server.entity.TblOrderForm;
import com.soms.server.entity.TblSupplierComponent;
import com.soms.server.entity.vo.ComponentVo;
import com.soms.server.entity.vo.SupplierComponentVo;
import com.soms.server.mapper.TblOrderFormMapper;
import com.soms.server.mapper.TblSupplierComponentMapper;
import com.soms.server.service.TblOrderFormService;
import com.soms.server.util.Algorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单表是订购商下单的表，内容以列存储的形式进行存储，同一订单编号视为同一订单里的内容。 服务实现类
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-15
 */

@Service
public class TblOrderFormServiceImpl extends ServiceImpl<TblOrderFormMapper, TblOrderForm> implements TblOrderFormService {

    @Autowired
    private TblOrderFormMapper tblOrderFormMapper;
    @Autowired
    private TblSupplierComponentMapper tblSupplierComponentMapper;
    @Override
    public Integer[] compoentRecomm(Map<Integer, Integer> typeandnum) throws Exception {
        QueryWrapper<TblSupplierComponent> wrapper = new QueryWrapper<TblSupplierComponent>();
        List<SupplierComponentVo> supplierComponentVos = new ArrayList<>();
        List<ComponentVo> componentVos = new ArrayList<ComponentVo>();
        //查询出生产零件的厂商
        for(Integer type:typeandnum.keySet()) {
            //零件种类参数构造
            {
                ComponentVo componentVo = new ComponentVo(type, typeandnum.get(type));
                 componentVos.add(componentVo);
            }
            wrapper.eq("compoent_id", type);
            List<TblSupplierComponent> components = tblSupplierComponentMapper.selectList(wrapper);
            if(components.isEmpty()){
                throw new Exception("部分零件没有厂商生产");
            }
            //零件工厂
            for(TblSupplierComponent supplier:components){
                SupplierComponentVo supplierComponentVo = new SupplierComponentVo(supplier.getComponentId(), supplier.getSupplierId(), supplier.getPrice());
                supplierComponentVos.add(supplierComponentVo);
            }
        }
        Map test = Algorithm.test(componentVos, supplierComponentVos, componentVos.size());

        return new Integer[0];
    }
}

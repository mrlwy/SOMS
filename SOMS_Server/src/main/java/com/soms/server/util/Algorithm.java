package com.soms.server.util;



import com.baomidou.mybatisplus.core.assist.ISqlRunner;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.soms.server.entity.TblComponent;
import com.soms.server.entity.TblSupplier;
import com.soms.server.entity.TblSupplierComponent;
import com.soms.server.entity.vo.ComponentVo;
import com.soms.server.entity.vo.SupplierComponentVo;
import com.soms.server.mapper.TblSupplierComponentMapper;
import com.soms.server.service.TblComponentService;
import com.soms.server.service.TblSupplierComponentService;
import com.soms.server.service.TblSupplierService;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Algorithm {
    @Autowired
    TblSupplierComponentMapper supplierComponentMapper;



    static int n = 0;                 //耗资cost为0  n为员工数量
    static BigDecimal cost = new BigDecimal(1000000000);
    static BigDecimal c[][]=new BigDecimal[10][10];  //各员工在各工作上的工资
    static List<SupplierComponentVo> list = new ArrayList<>();
    static List<SupplierComponentVo> sclist = new ArrayList<>();
    static List<SupplierComponentVo> work(List<ComponentVo> componentVos, int i, BigDecimal count , int x[]){


        if(i>n && count.compareTo(cost)==-1){//各个工作安排完毕且为更优解
            cost = count;//更新最优解
            list = showResult(componentVos,x);
            return showResult(componentVos,x);//展示方案
        }

        if(count.compareTo(cost)==-1)
            for(int j=1;j<=n;j++)
                if(x[j] == 0){  //j表示供应商
                    x[j] = i;     //i表示零件
                    count=count.add(c[j][i].multiply(new BigDecimal(componentVos.get(i-1).getCount())));
                    work(componentVos,i+1,count,x);
                    x[j] = 0;
                    count = count.subtract(c[j][i].multiply(new BigDecimal(componentVos.get(i-1).getCount())));
                }
        return list;
    }

      static List<SupplierComponentVo> showResult(List<ComponentVo> componentVos,int x[]) {
        BigDecimal pay= new BigDecimal(0);
        sclist = new ArrayList<>();
        for(int i=1;i<x.length;i++) {
            //System.out.println("       供应商         ("+i+")   零 件 ："+x[i]+"         价格 为 ："+c[i][x[i]]);
            pay = pay .add(c[i][x[i]]);

            //sclist.add(new SupplierComponentVo(x[i],i,new BigDecimal(componentVos.get(x[i]-1).getCount()).multiply(c[i][x[i]])));


            sclist.add(new SupplierComponentVo(x[i],i,c[i][x[i]],componentVos.get(x[i]-1).getCount()));

        }
          //System.out.println("       当    前    耗    资    为    ："+pay+"\n  ------------------------------------");


          return sclist;

    }

    // 参数number是零件种类的个数
    public static Map test(List<ComponentVo> componentVos, List<SupplierComponentVo> list1, int number) {
        // 在执行方法前，初始化一下
        Algorithm.n = 0;                 //耗资cost为0  n为员工数量
        Algorithm.cost = new BigDecimal(1000000000);

        n=number;
        int x[]=new int[n+1];//用于记录工作安排
        for(int i=1;i<=n;i++)  System.out.print(i+"  ");
        System.out.println();
        List<SupplierComponentVo> list= list1;
        for (int z=1;z<=n;z++) {
            for(int w=1;w<=n;w++){
                c[z][w] = new BigDecimal(10000);
            }
        }

       // 003零件3   零件5  零件6
        int m = 0;
        for(int i=1;i<=n;i++){

            for(int j=1;j<=n;j++){
                if(list.size()>m){
                if (list.get(m).getSupplierId()==i && list.get(m).getComponentId()==j) {//i=2  ,m = 3
                    c[i][j]=list.get(m).getPrice();//输入各个数据
                    m++;// m=1
                }
                }
                x[j] = 0;  //初始化工作安排
            }
            //cost+=c[i][i];  //初始耗资：按员工顺序安排工作
        }
        Map map = new HashMap();
        map.put("list",work(componentVos,1,new BigDecimal(0),x));
        map.put("cost",cost);
        return map;//i表示员工，从第1个员工安排工作
    }
}
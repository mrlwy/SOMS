package com.soms.server.mapper;

import com.soms.server.entity.TblSupplier;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 供应商表 Mapper 接口
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-11
 */
@Repository
public interface TblSupplierMapper extends BaseMapper<TblSupplier> {

}

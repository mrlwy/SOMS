package com.soms.server.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.soms.server.entity.TblLinkman;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.soms.server.entity.vo.LinkmanVo;
import com.soms.server.entity.vo.SupplierComponentVo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 供应商联系人表 Mapper 接口
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-23
 */
@Repository
public interface TblLinkmanMapper extends BaseMapper<TblLinkman> {
    Page<LinkmanVo> queryAllLinkman(Page<LinkmanVo> page);
    LinkmanVo queryLinkmanById(Integer id);
}
